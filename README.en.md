# This is not an autobiography…

Just a CV, with jobs-related stuff and whatnot. I'm also a human being, playing with my kids, discussing with family and friends, enjoying silence alone… We might get into that around a coffee some time soon, who knows? But, in the meantime…

# Work history

## Free Software Craftsman @ infoPiiaf, since may 2015

Working on energy efficiency for software projects thanks to agile methodologies and free software swarming approaches, targetting social and solidarity initiatives.

* Grown up fed with: positive parenting, nonviolent communication, infrastructure automation, business/market fit, government procurement…
* Inspired by Faber & Mazlish, Catherine Gueguen, Marshall Rosenberg, Sébastien Broca, Eric Raymond, Unix Sheikh…

### Free (as in freedom) software developer and maintainer

* [Filaé](https://filae.frama.io/), web-based software supporting French Forensic Medical Unit:
   * built and deployed at the Versailles public hospital since 2018 ;
   * presented at the Internation French-speaking symposium of medical forensics in 2021;
   * ideal candidate for swarming, promising discussions without happy endings yet.
* [OLIGAA](https://framagit.org/CDSA33/OLIGAAu), online tool for associations to manage news, local actors and activities:
   * built with the Gironde's sport committee for persons with intellectual disability since 2010;
   * freed in 2016, our client convinced by the swarming ethics and potential;
   * deployed by another committee only, but it's a start!

### On-demand software architecture and development

Various contributions for closed-source projects:

* Customer self-care web interface, French electric utility cooperative company and renewable energy producer;
* Coached e-learning management system, corporate training company with human impact;
* Data gathering architecture and visualisation software, connected health platform to prevent frailty for elderly people;
* Payroll software development for childminders, French caretaker-oriented company.

## IT Consultant @ OCTO Technology, sept. 2010 - apr. 2015

Working on IT architecture and software engineering, many domains concerned: retail banking, media, internet services…

### Software Padawan: completed.

Learning from the bests. Among them, special thanks go to Eric Pantera, Benjamin Magnan, Vincent Coste, Jean-Loup Yu and Julien Jakubowski.

* Grown up fed with: TDD, Clean Code, Continuous Integration, Agile methodologies, Kanban…
* Inspired by Andy Hunt, Uncle Bob Martin, Kent Beck…

### Software Knight: completed.

Working as an autonomous team member, actively contributing in continuous improvement and team efficiency. Special thanks go to Mathieu Gandin, Cyrille Deruel and Michel Domenjoud.

* Grown up fed with: software craftsmanship, legacy code, product ownership, feature teams, lean management…
* Inspired by Martin Fowler, Micheal Feathers, Henrik Kniberg, David J. Anderson…

### Software Master: never ending…

Supporting technical and human changes in team transformations. Addressing impacts on organisations and communication structures. Special thanks go to David Alia, Simon Lehericey and Hervé Lourdin.

* Grows up fed with: technical leadership, change management, entreprise integration patterns, scaling Agile…
* Inspired by John P. Kotter, Gerald M. Weinberg, Donald G. Reinertsen, Benjamin Zander…

## Teacher @ EPITA, june 2011 - july 2013

### Agile Methodologies and Developer Productivity

Combining theory and practice with slide decks, Q&A sessions and software project:
* first contacts with Agile: its tools, artefacts and rituals;
* productivity leveraging: continuous integration, TDD and ATDD…
* continuous improvement: principles, applied in iteration retrospective…
* feedback and opening: the project retrospective, Kanban, Lean, DevOps…

## Co-founder @ Hubluc, jan. 2011 - may 2013

Hubluc.com was a service built upon a real-time comparison engine of legal digital music providers. Wanna buy "Thriller" by Mickael Jackson? Find the best price or quality around french digital music providers.

Facing technical challenges was a day to day topic. Numerous integration strategies, heterogeneous quality of services and response times, data gathering… Analytics in wild sea.

But Hubluc.com was above all an intrapreneuring adventure. Building a vision, looking for a product/market fit, experimenting and validating (or not) hypotheses… Iterating.

Start-up of the [week 37-2011 (in French)](https://www.latribune.fr/blogs/la-start-up-internet-de-la-semaine/20110913trib000648862/le-premier-moteur-de-recherche-de-musique-numerique-a-debarque.html).

## Intern @ OCTO Technology, fev. 2010 - july 2010

Working on an innovative project related to legal music platforms. I guess you found out now what it became… ;-)

## Teaching Assistant @ EPITA, sept. 2007 - jan. 2010

Providing lectures and tutorials to younger students on software development: C, C++, Java…

## Mac Developer @ DirectStreams, sept. 2008 - jan. 2009

Working on a multimedia solution for premier international hotels providing IPTV, Video on Demand, Internet, Room services… On Apple computers in rooms.

## Chairman @ EpiMac, nov. 2007 - nov. 2008

Promoting Apple products through events, backed by Apple staff: ephemerial showrooms, thematic conferences, Cocoa training classes… Even free distributions of apples (yes, the fruit) in schools hallways!
