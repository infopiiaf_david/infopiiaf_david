[English version](https://framagit.org/infopiiaf_david/infopiiaf_david/-/blob/main/README.en.md).

# Ceci n'est pas une autobiographie…

Juste un CV, avec quelques références professionnelles et autres joyeusetés. Je suis également un être humain, jouant avec mes enfants, discutant avec ma famille et mes amis, profitant seul du silence… Nous pourrions être amenés à évoquer tout cela autour d'un café un de ces jours, qui sait ? Mais, en attendant…

# Réalisations professionnelles

## Artisan logiciel, libre et solidaire @ infoPiiaf, depuis 2015

Travaillant sur l'efficience énergétique des projets logiciels grâce aux méthodes agiles et à l'essaimage de logiciels libres, à destination des initiatives locales et solidaires.

* Grandi alimenté par : la parentalité positive, la communication NonViolente, l'automatisation de l'infrastructure, l'adéquation offre/marché, les marchés publics…
* Inspiré par Faber & Mazlish, Catherine Gueguen, Marshall Rosenberg, Sébastien Broca, Eric Raymond, Unix Sheikh…

### Développeur et mainteneur de logiciels libres

* [Filaé](https://filae.frama.io/), application web pour les UMJ (unités médico-judiciaires) :
   * construite et déployée à l'hôpital public de Versailles depuis 2018 ;
   * présentée au Congrès International Francophone de Médecine Légale en 2021 ;
   * candidat idéal pour l'essaimage, discussions prometteuses sans concrétisation pour le moment.
* [OLIGAA](https://framagit.org/CDSA33/OLIGAAu), outil en ligne facilitant la gestion des actualités, des acteurs locaux et des activités :
   * construite et déployée au comité départemental du sport adapté de Gironde depuis 2010 ;
   * libéré en 2016, notre client étant convaincu par notre démarche d'essaimage ;
   * déployé par un seul autre comité, mais c'est un début !

### Architecture et développement logiciel à la demande

Contributions diverses et variées à des projets au code propriétaire :

* Espace-client web, producteur et fournisseur d'électricité renouvelable coopératif ;
* Outil de e-learning coaché, société de formation professionnelle à impact humain ;
* Architecture de collecte de données et interface de visualisation, plateforme de santé connectée pour prévenir la fragilité des seniors ;
* Outil de gestion de la paie des assistantes maternelles, société orientée "aide à la personne".

## Consultant IT @ OCTO Technology, sept. 2010 - avril. 2015

Travaillant sur l'architecture IT et l’ingénierie logicielle, dans de nombreux domaines : banque de détail, médias, services en ligne…

### Padawan logiciel : accompli.

Apprenant des meilleurs. Parmi eux, remerciements particuliers à Éric Pantera, Benjamin Magnan, Vincent Coste, Jean-Loup Yu et Julien Jakubowski.

* Grandi alimenté par : la pratique du TDD, le Clean Code, l'intégration continue, les méthodes Agile, Kanban…
* Inspiré par Andy Hunt, Uncle Bob Martin, Kent Beck…

### Chevalier logiciel : accompli.

Travaillant comme coéquipier autonome, contribuant activement à l'amélioration continue et à l'efficience de l'équipe. Remerciements particuliers à Mathieu Gandin, Cyrille Deruel et Michel Domenjoud.

* Grandi alimenté par : software craftsmanship, legacy code, product ownership, feature teams, lean management…
* Inspiré par Martin Fowler, Micheal Feathers, Henrik Kniberg, David J. Anderson…

### Maître logiciel : sans fin…

Accompagnant les changements techniques et humains lors de transformations d'équipes. Traitant les impacts sur les structures d'organisation et de communication. Remerciements particuliers à David Alia, Simon Lehericey et Hervé Lourdin.

* Grandi alimenté par : le leadership technique, la gestion du changement, les patrons d'intégration logicielle en entreprise, l'Agile à l'échelle…
* Inspiré par John P. Kotter, Gerald M. Weinberg, Donald G. Reinertsen, Benjamin Zander…

## Enseignant @ EPITA, juin 2011 - juil. 2013

### Mise En Pratique Agile

Combinant théorie et pratique par cours magistraux, sessions de questions/réponses et projet logiciel :
* premiers contacts avec l'Agile: ses outils, résultats et rituels ;
* leviers de productivité : intégration continue, TDD et ATDD…
* amélioration continue : principes, mis en pratiques lors de rétrospectives d'itérations ;
* réactions et ouverture : rétrospective projet, Kanban, Lean, DevOps…

## Co-fondateur @ Hubluc, jan. 2011 - mai 2013

Hubluc.com était un service construit sur un moteur de comparaison des fournisseurs de musique numérique. Envie d'acheter "Thriller" de Mickael Jackson ? Trouvez le meilleur tarif ou la meilleure qualité chez les fournisseur français.

Se confronter à des défis techniques était une activité quotidienne. De nombreuses stratégies d'intégration, hétérogénéité des qualités de service et des temps de réponse, collecte de données… De l'analyse en eaux troubles.

Mais Hubluc.com était avant tout une aventure intrapreneuriale. Construire une vision, rechercher l'adéquation entre le produit et le marché, expérimenter et valider (ou pas) des hypothèses… Et itérer.

Start-up de [la semaine 37-2011](https://www.latribune.fr/blogs/la-start-up-internet-de-la-semaine/20110913trib000648862/le-premier-moteur-de-recherche-de-musique-numerique-a-debarque.html).

## Stagiaire @ OCTO Technology, fév. 2010 - juil. 2010

Travaillant sur un projet innovant en rapport avec les fournisseurs de musique numérique. J'imagine que vous avez deviné ce qu'il est devenu… ;-)

## Assistant-enseignant @ EPITA, sept. 2007 - jan. 2010

Dispensant cours et travaux pratiques aux étudiants des promotions suivantes : C, C++, Java…

## Développeur Mac @ DirectStreams, sept. 2008 - jan. 2009

Travaillant sur une solution multimédia pour l'hôtellerie de luxe internationale, proposant la TV sur IP, la vidéo à la demande, Internet, room services… Sur ordinateur Apple dans les chambres.

## Président @ EpiMac, nov. 2007 - nov. 2008

Promouvant les produits Apple au travers d'événements soutenus par Apple : présentations de produits, conférences thématiques, cours de développement Cocoa… Même des distributions gratuites de pommes dans les couloirs des écoles !
